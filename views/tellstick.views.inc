<?php
/**
* @file
* Handles the tellstick module interface to the views module.
*
* The interface makes it possible to use tellstick records in the
* views module.
*/

function tellstick_views_data() {
  $data = array();

  $data['tellstick_data']['table']['group'] = t('Tellstick');

  $data['tellstick_data']['table']['base'] = array(
    'field' => 'pid', // This is the identifier field for the view.
    'title' => t('Tellstick sensors'),
    'help' => t('Tellstick sensors dumpd data.'),
    'weight' => -10,
  );

  $data['tellstick_data']['data'] = array(
    'title' => t('Sensor data'),
    'help' => t('The Sensor data, depends on sensor'),
    'field' => array(
      'handler' => 'tellstick_handler_field_data',
      'click sortable' => TRUE
    ),
    //'filter' => array(
    //  'handler' => 'views_handler_filter',
    //),
    'sort' => array(
      'handler' => 'views_handler_sort'
    )
  );

  $data['tellstick_data']['pid'] = array(
    'title' => t('Record id'),
    'help' => t('Record identifyer'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort'
    )
  );

  $data['tellstick_data']['sid'] = array(
    'title' => t('Sensor id'),
    'help' => t('The sensor identity'),
    'field' => array(
      'handler' => 'tellstick_handler_field_sensor',
      'click sortable' => TRUE
    ),
    'filter' => array(
      'handler' => 'tellstick_handler_filter_sensor',
    ),
    'sort' => array(
      'handler' => 'tellstick_handler_sort_sensor'
    )
  );

  $data['tellstick_data']['created'] = array(
    'title' => t('Created'),
    'help' => t('Time when the record was created'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date'
    )
  );

  return $data;
}
