<?php
class tellstick_handler_sort_sensor extends views_handler_sort {
  function option_definition() {
    $options = parent::option_definition();

    $options['sort_type'] = array('default' => 'weight');

    return $options;
  }

  function options_form(&$form, &$form_state) {

    $form['sort_type'] = array(
      '#type' => 'radios',
      '#title' => t('Sensor sort order'),
      '#options' => array(
        'id' => t('By id'),
        'name' => t('By name'),
        'weight' => t('By weight'),
      ),
      '#description' => t('Sort by sensor id, name or by the same weight as used for the block.'),
      '#default_value' => $this->options['sort_type'],
    );
    parent::options_form($form, $form_state);
  }

  /**
   * Called to add the sort to a query.
   */
  function query() {
    $this->ensure_my_table();
    switch ($this->options['sort_type']) {
      case 'id':
      default:
        $this->query->add_orderby($this->table_alias, $this->real_field, $this->options['order']);
        return;
      case 'name':
        $this->query->add_orderby($this->table_alias, $this->real_field, $this->options['order']);
        return;
      case 'weight':
        $formula = views_date_sql_format('YmdH', "$this->table_alias.$this->real_field");
        return;
    }

    // Add the field.
    $this->query->add_orderby(NULL, $formula, $this->options['order'], $this->table_alias . '_' . $this->field . '_' . $this->options['sort_type']);
  }
}


/*     $this->value_title = t('Sensors');
    $options = array();
    $result = _tellstick_get_sensors();
    foreach ($result->children() as $sensor) {
      $attributes = $sensor->attributes();
      $options[(integer)$attributes['id']] = (string)$attributes['name'];
    }
    asort($options);
    $this->value_options = $options; */
