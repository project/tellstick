<?php
/**
* @file
* A handler to provide display for sensor ids.
*
* Allows for display of number or name.
*
* @ingroup views_field_handlers
*/

class tellstick_handler_field_sensor extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();

    $options['type'] = array('default' => 'name');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['type'] = array(
      '#type' => 'select',
      '#title' => t('Sensor presentation'),
      '#options' => array(
        'id' => t('By id'),
        'name' => t('By name'),
      ),
      '#default_value' => $this->options['type'],
    );
  }

  function render($values) {
    $value = $values->{$this->field_alias};

    switch ($this->options['type']) {
      case 'name':
      default:
        return _tellstick_get_sensor_value($value);
      case 'id':
        return $value;
    }
  }
}

function _tellstick_get_sensor_value($value) {
//  $result = db_query('SELECT ts.data
//    FROM {tellstick_data} ts WHERE ts.sid = :sid',
//    0, 1, array(':sid' => $value));
//  foreach ($result as $record) {
//    $temp_value = unserialize($record->data);
//  }

  $query = db_select('tellstick_data', 'td')
  ->condition('td.sid', $value, '=')
  ->fields('td',array('data'))
  ->range(0,1)
  ->orderBy('td.pid', 'DESC');
  $result = $query->execute();
  foreach ($result as $record) {
    $temp_value = unserialize($record->data);
  }
  return $temp_value['name'];
} 
