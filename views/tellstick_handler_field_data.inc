<?php
/**
* @file
* A handler to provide proper displays for data records.
*
* Allows for display of number or text.
*
* @ingroup views_field_handlers
*/

class tellstick_handler_field_data extends views_handler_field {

  const IGNORE_VALUES = 
      'id name lastUpdated ignored client clientName online editable keepHistory protocol model sensorId';

  function option_definition() {
    $options = parent::option_definition();

    $options['type'] = array('default' => 'formatted');
    $options['raw'] = array('default' => 'Undefined');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['type'] = array(
      '#type' => 'select',
      '#title' => t('Output format'),
      '#options' => array(
        'formatted' => t('Formatted'),
        'serialized' => t('Serialized'),
        'unserialized' => t('Unserialized'),
        'raw' => t('Raw'),
      ),
      '#description' => t('Formatted gives a value directly for outputting, raw gives a value that can be used for creating diagrams'),
      '#default_value' => $this->options['type'],
    );
    $form['raw'] = array(
      '#type' => 'select',
      '#title' => t('Raw data'),
      '#options' => $this->get_raw_options_available(),
      '#default_value' => $this->options['raw'],
      '#description' => t('Make sure to select a valid item for the sensor in question'),
      '#dependency' => array(
        'edit-options-type' => array('raw'),
      ),
    );
  }

  function render($values) {
    $value = $values->{$this->field_alias};

    switch ($this->options['type']) {
      case 'serialized':
        return check_plain($value);
      case 'unserialized':
        return check_plain(print_r(unserialize($value), TRUE));
      case 'raw':
        return tellstick_handler_field_data::get_data_raw($value);
      case 'formatted':
      default:
        return tellstick_handler_field_data::get_data_value($value);
    }

  }

  private function get_data_value($value) {
  
    $unserial = unserialize($value);
    $data = '';
    $dew_calc = array();
    $compare_string = self::IGNORE_VALUES;
    if ( variable_get('tellstick_battery_status', 0) == 0 ) {
      $compare_string = $compare_string . ' battery';
    } 
    foreach ($unserial as $key => $temp_value) {
      if ( strpos($compare_string, $key) === FALSE ) {
        $dew_calc[$key] = $temp_value;
        $data .= _tellstick_sensor_post(check_plain($key),
          check_plain($temp_value));
        if (!empty($data) ) { $data .= '<br />';}
      }
    }
    if ( ! empty($dew_calc['temp']) & ! empty($dew_calc['humidity'])) {
      $data .= _tellstick_sensor_post('dewpt',
        round(_tellstick_dew_point($dew_calc['temp'],
        $dew_calc['humidity']), 1));
      $data .= '<br />';
    }
    return $data;
  }

  private function get_data_raw($value) {
    $unserial = unserialize($value);
    $data = array();
    $dew_calc = array();
    $compare_string = self::IGNORE_VALUES;
    if ( variable_get('tellstick_battery_status', 0) == 0 ) {
      $compare_string = $compare_string . ' battery';
    } 
    foreach ($unserial as $key => $temp_value) {
      if ( strpos($compare_string, $key) === FALSE ) {
        $dew_calc[$key] = $temp_value;
        $data[check_plain($key)] = (double)check_plain($temp_value);
      }
    }
    if ( ! empty($dew_calc['temp']) & ! empty($dew_calc['humidity'])) {
      $data['dewpt'] =  round(_tellstick_dew_point($dew_calc['temp'],
        $dew_calc['humidity']), 1);
    }
    return empty($this->options['raw']) ? FALSE : $data[$this->options['raw']];
  }

  private function get_raw_options_available() {
    $options = array(
      /* Would be better to grab the info from the selected sensor
         but until then we do it predefined */
      'temp' => t('Temperature'),
      'humidity' => t('Humidity'),
      'dewpt' => t('Dew point'),
      'rrate' => t('Rain / hour'),
      'rday' => t('Rain / day'),
      'rtot' => t('Total rain'),
      'wavg' => t('Wind average'),
      'wgust' => t('Wind gust'),
      'wdir' => t('Wind direction'),
      'battery' => t('Battery'),
    );
    return $options;
  }

}

