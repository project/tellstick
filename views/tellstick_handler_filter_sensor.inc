<?php
class tellstick_handler_filter_sensor extends views_handler_filter_in_operator {

  function get_value_options() {

    if (isset($this->value_options)) {
      return;
    }

    $this->value_title = t('Sensors');
    $options = array();
    $result = _tellstick_get_sensors();
    foreach ($result->children() as $sensor) {
      $attributes = $sensor->attributes();
      $options[(integer)$attributes['id']] = (string)$attributes['name'];
    }
    asort($options);
    $this->value_options = $options;
  }
}
