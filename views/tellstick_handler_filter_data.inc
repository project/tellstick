<?php
class tellstick_handler_filter_data extends views_handler_filter_in_operator {

  function get_value_options() {

    if (isset($this->value_options)) {
      return;
    }

    $this->value_title = t('Sensor data');
    $options = array();

    $options['temp'] = 'Temperature';
    $options['wavg'] = 'Wind: Average';
    $options['wgust'] = 'Wind: Gust';
    $options['wdir'] = 'Wind: Direction';
    $options['rrate'] = 'Rain: Rate';
    $options['rtot'] = 'Rain: Total';
    $options['battery'] = 'Battery: Voltage';

    $this->value_options = $options;
  }
}
