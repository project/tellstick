<?php

/**
 * @file
 * Token callbacks for the tellstick_token module.
 */

/**
 * Implements hook_token_info().
 *
 * @ingroup tellstick_token
 */
function tellstick_token_info() {

  $type = array(
    'name' => t('Tellstick'),
    'description' => t('Tokens related to Tellstick sensors.')
  );

  // Create list of all sensors
  $sensor_list = '';
  $sxe = _tellstick_get_sensors();
  foreach($sxe->children() as $child) {
    $sensors = $child->attributes();
    $sensor_list .= (string)$sensors['id'];
    $sensor_list .= ' (' . (string)$sensors['name'] . '), ';
  }

  $info['sensor'] = array(
    'name' => t('Sensor'),
    'description' => t('Value from sensors. Valid sensors: @sensor_list' .
                       ' Without a type the sensor name is returned. For a value the type of the value must be given like temp humidity.',
      array('@sensor_list' => $sensor_list)),
    'dynamic' => TRUE,
  );

  return array(
    'types' => array('tellstick' => $type),
    'tokens' => array(
      'tellstick' => $info,
    ),
  );
}

/**
 * Implements hook_tokens().
 *
 * @ingroup tellstick_token
 */
function tellstick_tokens($type, $tokens, array $data = array(), array $options = array()) {

  $url_options = array('absolute' => TRUE);
  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
    $language_code = $options['language']->language;
  }
  else {
    $language_code = NULL;
  }
  $sanitize = !empty($options['sanitize']);

  $replacements = array();

  // Tellstick tokens.
  if ($type == 'tellstick' ) {

    if ($sensor_tokens = token_find_with_prefix($tokens, 'sensor')) {
      foreach ($sensor_tokens as $part => $original) {
        // Complicated way to avoid warnings about missing array
        $token_array = explode(':', $part, 2);
        $id = $token_array[0];
        $type = empty($token_array[1]) ? NULL : $token_array[1];
//            print('<pre>');
//            print_r($token_array);
//            print('</pre>');
        $sensor = _tellstick_get_sensor_data($id);
        if (!empty($type)) {
          $data = $sensor->data->attributes();
          foreach ($sensor->data as $name ) {
            if ( $name->attributes()->name == $type) {
              $datastring = _tellstick_sensor_post( 
                (string)$name->attributes()->name, 
                (string)$name->attributes()->value );
            }
          }
        }
        else {
          $datastring = trim((string)$sensor->name);
        }
        $replacements[$original] = $sanitize ? 
          filter_xss($datastring) : $datastring;
      }
    }
  }
  return $replacements;
}
