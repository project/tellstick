<?php

/**
 * @file
 * Administrative page callbacks for the Telldus Live! module.
 */

/**
 * Page callback: Form constructor for a form to list and reorder sensors.
 *
 * @ingroup forms
 * @see tellstick_menu()
 * @see tellstick_admin_overview_submit()
 */
function tellstick_admin_overview($form) {
  // Get all current sensors.
  $tellstick_sensors = _tellstick_get_sensors();

  $form['#tree'] = TRUE;
  foreach ($tellstick_sensors->children() as $order => $sensor) {

    $attributes = $sensor->attributes();
    $id = (string)$attributes['id'];
    $default_value = _tellstick_variable_set($id);
    if (empty($attributes['name']) ) {
      continue;
    }

    $form['tellstick_sensors'][$id]['name'] = array('#markup' => check_plain((string)$attributes['name'] ));
    $form['tellstick_sensors'][$id]['status'] = array('#markup' => $default_value['active']?t('enabled'):t('disabled') );
    $form['tellstick_sensors'][$id]['configure'] = array('#type' => 'link', '#title' => t('configure'), '#href' => 'admin/config/content/tellstick/' . $id);
    $form['tellstick_sensors'][$id]['active'] = array('#type' => 'link', '#title' => $default_value['active']?t('disable'):t('enable'), '#href' => 'admin/config/content/tellstick/' . $id . '/toggle' );
    $form['tellstick_sensors'][$id]['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight for @title', array('@title' => (string)$attributes['name'])),
      '#title_display' => 'invisible',
      '#default_value' => $default_value['weight'],
    );
    /* Prepare for sorting of array */
    $form['tellstick_sensors'][$id]['#weight'] = $default_value['weight'];
  }

  uasort($form['tellstick_sensors'], "element_sort");
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save changes'));
  return $form;
}

/**
 * Form submission handler for tellstick_admin_overview().
 */
function tellstick_admin_overview_submit($form, &$form_state) {
  foreach ($form_state['values']['tellstick_sensors'] as $id => $data) {
    if (is_array($data) && isset($data['weight'])) {
      // Only update if this is a form element with weight.
      _tellstick_variable_set($id, $data['weight'], 'weight');
    }
  }
  cache_clear_all('tellstick', 'cache_block', TRUE);
  drupal_set_message(t('The sensor ordering has been saved.'));
}

/**
 * Returns HTML for the sensor administration overview form.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 *
 * @ingroup themeable
 */
function theme_tellstick_admin_overview($variables) {
  $form = $variables['form'];

  $rows = array();
  foreach (element_children($form['tellstick_sensors']) as $id) {
    $form['tellstick_sensors'][$id]['weight']['#attributes']['class'] = array('tellstick-sensor-order-weight');
    $rows[] = array(
      'data' => array(
        drupal_render($form['tellstick_sensors'][$id]['name']),
        drupal_render($form['tellstick_sensors'][$id]['weight']),
        drupal_render($form['tellstick_sensors'][$id]['status']),
        drupal_render($form['tellstick_sensors'][$id]['configure']),
        drupal_render($form['tellstick_sensors'][$id]['active']),
      ),
      'class' => array('draggable'),
    );
  }
  $header = array(t('Sensor'), t('Weight'), t('Status'), array('data' => t('Operations'), 'colspan' => 2));
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'tellstick-sensor-order')));
  $output .= drupal_render_children($form);

  drupal_add_tabledrag('tellstick-sensor-order', 'order', 'sibling', 'tellstick-sensor-order-weight');

  return $output;
}

/**
 * Form constructor for the tellstick sensor toggle confirmation form.
 *
 * @param $format
 *   An object representing a sensor.
 *
 * @see tellstick_menu()
 * @see tellstick_admin_toggle_submit()
 * @ingroup forms
 */
function tellstick_admin_toggle($form, &$form_state, $sensor) {
  $form['#sensor'] = $sensor;
  return confirm_form($form,
    t('Are you sure you want to toggle visability of the %sensor sensor?', array('%sensor' => $sensor['name'])),
    'admin/config/content/tellstick/layout',
    t('Disabled sensors will not be displayed in the block.'),
    t('Toggle')
  );
}

/**
 * Form submission handler for tellstick_admin_toggle().
 */
function tellstick_admin_toggle_submit($form, &$form_state) {
  $sensor = $form['#sensor'];
  $id = $sensor['id'];
  $active = _tellstick_variable_set($id);
  _tellstick_variable_set($id, ! $active['active'], 'active');

  cache_clear_all('tellstick', 'cache_block', TRUE);

  drupal_set_message(t('Toggled the %sensor sensor.', array('%sensor' => $sensor['name'])));

  $form_state['redirect'] = 'admin/config/content/tellstick/layout';
}

/**
 * Page callback: Current posts settings
 *
 * @see tellstick_menu()
 */
function tellstick_admin_form($form, &$form_state) {

  $form['tellstick_live_credentials'] = array(
    '#type' => 'fieldset',
    '#title' => t('Telldus Live! credentials'),
  );
  $form['tellstick_live_credentials']['tellstick_information'] = array(
    '#markup' => '<p>' . t('Please get your own set of keys at') . ' ' .
     l('http://api.telldus.com/keys/index', 'http://api.telldus.com/keys/index') . '</p>'
  );
  $form['tellstick_live_credentials']['tellstick_public_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Tellstick Public Key'),
    '#default_value' => variable_get('tellstick_public_key', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Your Tellstick Public Key.'),
    '#required' => TRUE,
  );
  $form['tellstick_live_credentials']['tellstick_private_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Tellstick Private Key'),
    '#default_value' => variable_get('tellstick_private_key', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Your Tellstick Private Key.'),
    '#required' => TRUE,
  );
  $form['tellstick_live_credentials']['tellstick_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Tellstick Token'),
    '#default_value' => variable_get('tellstick_token', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Your Tellstick Token.'),
    '#required' => TRUE,
  );
  $form['tellstick_live_credentials']['tellstick_token_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Tellstick Token Secret'),
    '#default_value' => variable_get('tellstick_token_secret', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t('Your Tellstick Token Secret.'),
    '#required' => TRUE,
  );
  $form['tellstick_live_access'] = array(
    '#type' => 'fieldset',
    '#title' => t('Telldus Live! access method'),
  );
  $form['tellstick_live_access']['tellstick_access_method'] = array(
    '#type' => 'radios',
    '#title' => t('Select Telldus Live! access method'),
    '#options' => array(0 => 'http://', 1 => 'https://'),
    '#default_value' => variable_get('tellstick_access_method',0),
    '#description' => t('Select your prefered access method. Please NOTE that https:// might lock up your Drupal if your certificate is not working!'),
  );
  $form['tellstick_database_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Telldus Live! database settings'),
  );
  $form['tellstick_database_settings']['tellstick_database_lifetime'] = array(
    '#type' => 'select',
    '#title' => t('Select Telldus Live! database lifetime'),
    '#options' => array(0 => 'Forever', 1 => '1 day', 7 => '1 week', 
                        30 => ' 1 month', 90 => '3 months',
                        180 => '6 months'),
    '#default_value' => variable_get('tellstick_database_lifetime',0),
    '#description' => t('Select for how long you will keep your sensors data in the database. Requres an active cron to work.'),
  );
  return system_settings_form($form);
}

/**
 * Implements validation from the Form API.
 *
 * @param $form
 *   A structured array containing the elements and properties of the form.
 * @param $form_state
 *   An array that stores information about the form's current state
 *   during processing.
 */
function tellstick_admin_form_validate($form, &$form_state){

  $consumer = new HTTP_OAuth_Consumer(
    $form_state['values']['tellstick_public_key'],
    $form_state['values']['tellstick_private_key'],
    $form_state['values']['tellstick_token'],
    $form_state['values']['tellstick_token_secret']
  );
  $params = array();

  try {
    $access_method = ($form_state['values']['tellstick_access_method'] == 0) ?
      constant('REQUEST_URI') :
      constant('REQUEST_URI_S');
    $response = $consumer->sendRequest($access_method .
      '/user/profile',
      $params,
      'GET'
    );
    $sxe = new SimpleXMLElement($response->getBody()); 
    $valid_user = FALSE;
    $content = '';

    if ($sxe->getName() == 'user') {
      foreach($sxe->children() as $child => $user_info) {
        switch($child) {
          case 'firstname':
          case 'lastname':
            $content .= check_plain($user_info) . ' ';
            break;
          case 'email':
            $content .= ' ' . t('with email:') . ' ' . check_plain($user_info);
            $valid_user = TRUE;
            break;
        }
      }
    }
    if ( $valid_user ) {
      drupal_set_message(t('These credentials are valid for ') . $content );
    }
    else { 
      form_set_error('tellstick_public_key', t('You must supply a valid Public Key.'));
      form_set_error('tellstick_private_key', t('You must supply a valid Private Key.'));
      form_set_error('tellstick_token', t('You must supply a valid Token.'));
      form_set_error('tellstick_token_secret', t('You must supply a valid Token Secret.'));
    }
  } catch (Exception $e){
    variable_set('tellstick_access_method',0);
    form_set_error('tellstick_access_method', t('The access method you selected does not work. Please change.') . '<br />' . $e->getMessage() );
  }
}

/**
 * Page callback: Current posts settings
 *
 * @see tellstick_menu()
 */
function tellstick_sensors_form($form, &$form_state, $sensor = NULL) {

  if (isset($sensor)) {
    $form['tellstick_no_specific'] = array(
      '#type' => 'markup',
      '#markup' => 'No specific setings for this sensor! Displaying the generic settings instead.',
    );
  }
  $form['tellstick_live_windsensors'] = array(
    '#type' => 'fieldset',
    '#title' => t('Telldus Live! Wind sensors'),
  );
  $form['tellstick_live_windsensors']['tellstick_offset'] = array(
    '#type' => 'textfield',
    '#title' => t('Wind direction offset'),
    '#size' => 4,
    '#maxlength' => 10,
    '#default_value' => variable_get('tellstick_offset',0),
    '#description' => t('Add an offset, in degrees, to the Wind Direction. This is useful if you can\'t mount it facing north.'),
  );
  $form['tellstick_live_thermometers'] = array(
    '#type' => 'fieldset',
    '#title' => t('Telldus Live! Thermometers'),
  );
  $form['tellstick_live_thermometers']['tellstick_temperature_unit'] = array(
    '#type' => 'radios',
    '#title' => t('Temperature unit'),
    '#options' => array(0 => 'Celsius', 1 => 'Fahrenheit', 2 => 'Kelvin'),
    '#default_value' => variable_get('tellstick_temperature_unit',0),
    '#description' => t('Select unit for all thermometers. (Kelvin assumes you have selected Celsius on your units.)'),
  );
  $form['tellstick_live_lost_sensor_timeout'] = array(
    '#type' => 'fieldset',
    '#title' => t('Telldus Live! Sensor timeout'),
  );
  $form['tellstick_live_lost_sensor_timeout']['tellstick_sensor_timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('Sensor timeout'),
    '#size' => 4,
    '#maxlength' => 10,
    '#default_value' => variable_get('tellstick_sensor_timeout','3600'),
    '#description' => t('After how long time, in seconds, will a sensor be grayed out when there has been no signal received from it.'));
  $form['tellstick_live_battery_status'] = array(
    '#type' => 'fieldset',
    '#title' => t('Telldus Live! Battery status'),
  );
  $form['tellstick_live_battery_status']['tellstick_battery_status'] = array(
    '#type' => 'radios',
    '#title' => t('Battery status'),
    '#options' => array(0 => 'Do not show status', 1 => 'Show the status, if supported'),
    '#default_value' => variable_get('tellstick_battery_status',0),
    '#description' => t('Decide if you want to show the battery status together with the sensor values in views. The sensor name is always shown in red if the battery level is too low.'),
  );
  $form['tellstick_live_battery_status']['tellstick_battery_level'] = array(
    '#type' => 'textfield',
    '#title' => t('Battery alarm level'),
    '#size' => 4,
    '#maxlength' => 10,
    '#default_value' => variable_get('tellstick_battery_level',127),
    '#description' => t('The battery level for which the low battery indicator shall be triggered.'),
  );
  return system_settings_form($form);
}
