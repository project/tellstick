<?php

/**
 * @file
 * Contains install and update functions for tellstick.
 */

/**
 * Implements hook_install for tellstick.
 */
function tellstick_install() {
  // Create my tables.
  drupal_install_schema('tellstick');
}

/**
 * Implements hook_requirements
 *
 * Check if all necessary packages are installed
 */
function tellstick_requirements($phase) {

  // Check if PEAR HTTP_OAuth is installed
  // (SimpleXML is already a requirement of Drupal
  //  so that we don't need to check for)

  $requirements= array();

  if ($phase == 'install') {

    @require_once 'HTTP/OAuth/Consumer.php';

    if (! class_exists('HTTP_OAuth_Consumer')) {

      $requirements['tellstick'] = array(
        'title' => 'Tellstick',
        'value' => 'Tellstick need HTTP_OAuth installed. Please run "pear install HTTP_OAuth" from your command line.',
        'severity' => REQUIREMENT_ERROR,
      );
    }
  }
  return $requirements;
}

/**
 * Implements hook_uninstall
 */
function tellstick_uninstall() {

  // Delete all variables
  variable_del('tellstick_public_key');
  variable_del('tellstick_private_key');
  variable_del('tellstick_token');
  variable_del('tellstick_token_secret');
  variable_del('tellstick_previous_reset');
  variable_del('tellstick_access_method');
  variable_del('tellstick_offset');
  variable_del('tellstick_temperature_unit');
  variable_del('tellstick_database_lifetime');

  /* Remove each sensors counter */
  $num_deleted = db_delete('variable')
    ->condition('name', 'tellstick_rain_midnight_', 'LIKE')
    ->execute;  
  $num_deleted = db_delete('variable')
    ->condition('name', 'tellstick_sensor_', 'LIKE')
    ->execute; 
  // Drop my tables.
  drupal_uninstall_schema('tellstick'); 
}

function tellstick_schema() {
  $schema['tellstick_data'] = array(
    'description' => t('The table for remembering sensor info and statistics.'),
    'fields' => array(
      'pid' => array(
        'description' => t('The post identifier.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE),
      'sid' => array(
        'description' => t('The sensor identifier.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE),
      'data' => array(
        'description' => t('The current serialized data.'),
        'type' => 'varchar',
        'length' => 1024,
        'serialize' => TRUE),
      'created' => array(
        'description' => t('The UNIX timestamp for the data.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0),
      ),
    'primary key' => array('pid'),
  ); 
  return $schema;
}

/**
 * Installing tellstick sensors database
*/
function tellstick_update_7100() {
  drupal_install_schema('tellstick');
  return t('Installed the Tellstick Sensor scheme');
}


/**
 * Renamiing the tellstick_sensor table to tellstick_data
*/
function tellstick_update_7101() {
 $result = db_query("ALTER TABLE tellstick_sensor RENAME TO tellstick_data");
  return t('Renamed the Tellstick Data table');
}


